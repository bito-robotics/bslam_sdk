#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import rospy
import json
from crccheck.crc import Crc32
import struct
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
import config
from tcp_client import SlamTcpClient

class  DealMsg(object):
    def __init__(self):
        # check ip in sdk_node.py already
        self.server_ip = rospy.get_param('/bslam_node/server_ip')
        self.conn = SlamTcpClient(self.server_ip, port=config.feedback_port)

    def odom_wheel(self, msg: Odometry):
        json_bytes = json.dumps({
            "timestamp": int(msg.header.stamp.to_nsec()),
            "x": float(msg.pose.pose.position.x),
            "y": float(msg.pose.pose.position.y),
            "z": float(msg.pose.pose.position.z),
            "vx": float(msg.twist.twist.linear.x),
            "vy": float(msg.twist.twist.linear.y),
            "vw": float(msg.twist.twist.angular.z)
        }).encode('utf-8')
        REQ_MSG = b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x05\x00' + \
                  struct.pack('<I', (4 + len(json_bytes)))+ \
                  b'\x00\x00\x00\x00\x00\x00' + json_bytes
        crc = Crc32.calc(bytearray(REQ_MSG))
        REQ_MSG += struct.pack('<I', crc)  # The address mode in protocol is little-ending.

        self.conn.send_data(REQ_MSG)

    def imu_data(self, msg: Imu):
        json_bytes = json.dumps({
            "timestamp": int(msg.header.stamp.to_nsec()),
            "wx": float(msg.angular_velocity.x),
            "wy": float(msg.angular_velocity.y),
            "wz": float(msg.angular_velocity.z),
            "ax": float(msg.linear_acceleration.x),
            "ay": float(msg.linear_acceleration.y),
            "az": float(msg.linear_acceleration.z)
        }).encode('utf-8')
        REQ_MSG = b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x04\x00' + \
                  struct.pack('<I', (4 + len(json_bytes))) + \
                  b'\x00\x00\x00\x00\x00\x00' + json_bytes
        crc = Crc32.calc(bytearray(REQ_MSG))
        REQ_MSG += struct.pack('<I', crc)  # The address mode in protocol is little-ending.

        self.conn.send_data(REQ_MSG)


