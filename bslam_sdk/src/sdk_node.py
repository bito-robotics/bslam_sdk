#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# CheckParam()
# DealData()
# PackResult()
# Clear()

NAME = 'bslam_node'

import rospy
import json
import struct
import math
import config
from crccheck.crc import Crc32
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import LaserScan
from bslam_sdk.srv import *
from bslam_sdk.msg import *

from tcp_client import SlamTcpClient
from tcp_to_topic import DealData
from topic_to_tcp import DealMsg

def RPY2Quar(rx, ry, rz):
    cy = math.cos(rz * 0.5)
    sy = math.sin(rz * 0.5)
    cp = math.cos(ry * 0.5)
    sp = math.sin(ry * 0.5)
    cr = math.cos(rx * 0.5)
    sr = math.sin(rx * 0.5)

    w = cy * cp * cr + sy * sp * sr
    x = cy * cp * sr - sy * sp * cr
    y = sy * cp * sr + cy * sp * cr
    z = sy * cp * cr - cy * sp * sr

    return x, y, z, w

def check_ip(ipaddr):
    addr = ipaddr.strip().split('.')  # 切割IP地址为一个列表

    if len(addr) != 4:  # 切割后列表必须有4个参数
        rospy.logerr("check ip address failed1!")

    for i in range(4):
        try:
            addr[i] = int(addr[i])  # 每个参数必须为数字，否则校验失败
        except:
            rospy.logerr("check ip address failed1!")

        if addr[i] <= 255 and addr[i] >= 0:  # 每个参数值必须在0-255之间
            pass
        else:
            rospy.logerr("check ip address failed1!")

class BslamPub():
    def __init__(self):
        try:
            self.frame_id = rospy.get_param('/bslam_node/frame_id')
            self.server_ip = rospy.get_param('/bslam_node/server_ip')
            check_ip(self.server_ip)
            self.use_monitor = rospy.get_param('/bslam_node/use_monitor')
            self.pub_options = [rospy.get_param('/bslam_node/pub_pose'),
                            rospy.get_param('/bslam_node/pub_localization_state'),
                            rospy.get_param('/bslam_node/pub_match_score'),
                            rospy.get_param('/bslam_node/pub_dtc_data')]
            self.use_odom = rospy.get_param('/bslam_node/use_odom')
            self.use_imu = rospy.get_param('/bslam_node/use_imu')
        except Exception as e:
            rospy.logerr(rospy.get_param_names())
            raise Exception(e)

        self.pub_pose = rospy.Publisher(config.slam_pose_topic, Odometry, queue_size=1,
                                        tcp_nodelay=True)
        self.pub_state = rospy.Publisher(config.slam_state_topic, Int8, queue_size=1,
                                         tcp_nodelay=True)
        self.pub_match = rospy.Publisher(config.match_score_topic, Float64, queue_size=1,
                                         tcp_nodelay=True)
        self.pub_dtc = rospy.Publisher(config.dtc_topic, BitographerDTC, queue_size=1,
                                       tcp_nodelay=True)

        self.deal_data_func = DealData(self.frame_id, monitor_data=self.use_monitor)

        if self.pub_options == [False, False, False, False]:
            pass
        elif self.pub_options == [True, False, False, False]:
            self.req_msg = config.slam_pose_req
            self.deal_data = self.deal_data_func.only_pose
            self.publisher = [self.pub_pose]
        elif self.pub_options == [False, True, False, False]:
            self.req_msg = config.slam_state_req
            self.deal_data = self.deal_data_func.only_state
            self.publisher = [self.pub_state]
        elif self.pub_options == [False, False, True, False]:
            self.req_msg = config.match_score_req
            self.deal_data = self.deal_data_func.only_match
            self.publisher = [self.pub_match]
        elif self.pub_options == [True, True, True, False]:
            self.req_msg = config.merged_data_req
            self.deal_data = self.deal_data_func.merged_data
            self.publisher = [self.pub_pose, self.pub_state, self.pub_match]
            rospy.logwarn('Will not pub dtc, because param /pub_dtc_data is False')
        elif self.pub_options == [True, True, True, True]:
            self.req_msg = config.merged_data_with_dtc_req
            self.deal_data = self.deal_data_func.merged_data_with_dtc
            self.publisher = [self.pub_pose, self.pub_state, self.pub_match, self.pub_dtc]
        else:
            rospy.logerr('cannot deal with pub_options: {}'.format(self.pub_options))
            raise Exception(KeyError, 'pub_options err')

        self.pub_func = DealMsg()

        while True:
            try:
                self.tcp_conn = SlamTcpClient(self.server_ip)
                self.tcp_conn.send_data(self.req_msg)
                break
            except OSError :
                rospy.logerr('Cannot connect to tcp server {}, retry ...'.format(self.server_ip))
                continue

    def send_odom_to_tcp(self, data):
        tcp_msg = self.pub_func.odom_wheel(data)
        self.tcp_conn.send_data(tcp_msg)

    def send_imu_to_tcp(self, data):
        tcp_msg = self.pub_func.imu_data(data)
        self.tcp_conn.send_data(tcp_msg)

    def transform_data(self):
        data = self.tcp_conn.recv_data()
        unpack_data = self.deal_data(data)
        for idx, msg in enumerate(unpack_data):
            self.publisher[idx].publish(msg)

class ResetPoserServer():
    def __init__(self):
        self.server_ip = rospy.get_param('/bslam_node/server_ip')
        check_ip(self.server_ip)

        self.tcp_conn = SlamTcpClient(self.server_ip, port=config.single_port)

    def reset_pose(self, req):
        # {"x": 1.500, "y": 2.5, "z": 3, "qx": 0, "qy": 0, "qz": 1, "qw": 0}
        qx, qy, qz, qw = RPY2Quar(req.robot_rx, req.robot_ry, req.robot_rz)
        json_data_str = json.dumps({
            "x": float(req.robot_tx),
            "y": float(req.robot_ty),
            "z": float(req.robot_tz),
            "qx": float(qx),
            "qy": float(qy),
            "qz": float(qz),
            "qw": float(qw)})
        rospy.loginfo('call reset_pose srv through data: {}'.format(json_data_str))

        json_bytes = bytes(json_data_str, 'utf8')
        # 55 aa 55 aa 01 14 01 00 01
        REQ_MSG = b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x01\x00' + \
                  (4 + len(json_bytes)).to_bytes(4, 'little') + \
                  b'\x00\x00\x00\x00\x00\x00' + json_bytes
        crc = bytes.fromhex(Crc32.calchex(REQ_MSG))
        REQ_MSG += bytes(reversed(crc))  # The address mode in protocol is little-ending.

        self.tcp_conn.send_data(REQ_MSG)

        back_msg = self.tcp_conn.recv_data()

        length_data = back_msg[10:14]
        msg_length = int(length_data[::-1].hex(), 16)
        unpacked_values = str(back_msg[20:16 + msg_length], 'utf-8')

        return json.loads(unpacked_values)

def main():
    rospy.loginfo('Initialize node ...')
    rospy.init_node(NAME)

    rospy.loginfo('check ros params and init slam client')
    bslam_client = BslamPub()

    if bslam_client.use_odom:
        rospy.loginfo('start to sub odom topic: {}'.format(config.odom_topic))
        rospy.Subscriber(config.odom_topic, Odometry, bslam_client.pub_func.odom_wheel)
    else:
        rospy.logwarn('not send odom data to bslam')

    if bslam_client.use_imu:
        rospy.loginfo('start to sub imu topic: {}'.format(config.imu_topic))
        rospy.Subscriber(config.imu_topic, Imu, bslam_client.pub_func.imu_data)
    else:
        rospy.logwarn('not send imu data to bslam')

    reset_pose_server = ResetPoserServer()
    rospy.Service('reset_pose', ResetPose, reset_pose_server.reset_pose)

    while not rospy.is_shutdown():
        try:
            bslam_client.transform_data()
        except Exception as e:
            rospy.logwarn(e)
            continue

if __name__ == '__main__':
    main()
