#!/usr/bin/env python3

# CheckParam()
# DealData()
# PackResult()
# Clear()

NAME = 'map_node'
#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import rospy
import requests
import json
from std_msgs.msg import String
import config

class MapPub():
    def __init__(self):
        try:
            self.server_ip = rospy.get_param('/{}/server_ip'.format(NAME))
        except Exception as e:
            rospy.logerr(rospy.get_param_names())
            raise Exception(e)

        self.pub_map_name = rospy.Publisher(config.map_name_topic, String, queue_size=1,
                                       tcp_nodelay=True)

        self.map_list_url = 'http://' + str(self.server_ip) + ':8989/api/lidar/list'

    def get_map_in_use(self):
        response = requests.request('GET', self.map_list_url)
        res = json.loads(response.text)
        map_list = res['data']
        for map in map_list:
            if map['is_load']:
                return map['pcd_url']
        return ''

def main():
    rospy.loginfo('Initialize node ...')
    rospy.init_node(NAME)

    rospy.loginfo('check ros params and init map client')
    map_client = MapPub()

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        rate.sleep()
        try:
            map_in_use = map_client.get_map_in_use()
            map_client.pub_map_name.publish(map_in_use)
        except Exception as e:
            rospy.logwarn(e)
            continue

if __name__ == '__main__':
    main()
