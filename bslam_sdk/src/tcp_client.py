#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import socket
from config import continue_port

class SlamTcpClient(object):

    def __init__(self, server_ip, port=continue_port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((server_ip, port))
        self.sock.settimeout(2)

    def send_data(self, data):
        self.sock.sendall(data)

    def recv_data(self):
        return self.sock.recv(262144)

    def disconnect(self):
        self.sock.close()
