#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import rospy
import json
import struct
from nav_msgs.msg import Odometry
from std_msgs.msg import Float64
from std_msgs.msg import Int8
from sensor_msgs.msg import LaserScan
from bslam_sdk.msg import BitographerDTC
from config import slam_frame_id

class DealData(object):
    def __init__(self, frame_id, monitor_data=False):
        self.odometry = Odometry()
        self.slam_state = Int8()
        self.match_score = Float64()
        self.laser_scan = LaserScan()
        self.dtc_list = BitographerDTC()

        self.frame_id = frame_id
        self.msg_stamp = 100000000
        self.local_stamp = rospy.get_time()
        self.use_monitor = monitor_data
        if not self.use_monitor:
            rospy.logwarn('not monitor data, because param "/use_monitor" is False')

    def monitor_timestamp(self, timestamp):
        if self.use_monitor:
            msg_duration = (timestamp - self.msg_stamp) / 1000000000.0
            rev_duration = rospy.get_time() - self.local_stamp

            if msg_duration > 0.3:
                rospy.logwarn('Msg tmestamp timeout: {}'.format(msg_duration))
                self.dtc_list.dtc.append(10000)
            self.msg_stamp = timestamp

            if rev_duration > 0.3:
                rospy.logwarn('Receive timestamp timeout: {}'.format(rev_duration))
                self.dtc_list.dtc.append(10001)
            self.local_stamp = rospy.get_time()

    def only_pose(self, data):
        length_data = data[10:14]
        msg_length = int(length_data[::-1].hex(), 16)
        # print('Received {} byte: {}'.format(len(data), data[20: 16 + msg_length]))
        unpacked_values = str(data[20:16 + msg_length], 'utf-8')
        json_values = json.loads(unpacked_values)

        # example data
        # {"timestamp":1344509504,
        # "x":5.456111,
        # "y":6.345611,
        # "z":1.123456,
        # "qx":1.000000,
        # "qy":3.000000,
        # "qz":5.000000,
        # "qw":6.666677,
        # "vx":4.111111,
        # "vy":5.344500,
        # "vz":2.000000,
        # "wx":1.000000,
        # "wy":2.000000,
        # "wz":3.000000}
        timestamp, x, y, z, qx, qy, qz, qw, vx, vy, vz, wx, wy, wz = json_values
        self.monitor_timestamp(timestamp)

        self.odometry.header.stamp = rospy.Time.now()
        self.odometry.header.frame_id = slam_frame_id
        self.odometry.child_frame_id = self.frame_id
        self.odometry.pose.pose.position.y = y
        self.odometry.pose.pose.position.z = z
        self.odometry.pose.pose.orientation.x = qx
        self.odometry.pose.pose.orientation.y = qy
        self.odometry.pose.pose.orientation.z = qz
        self.odometry.pose.pose.position.x = x
        self.odometry.pose.pose.orientation.w = qw
        self.odometry.twist.twist.linear.x = vx
        self.odometry.twist.twist.linear.y = vy
        self.odometry.twist.twist.linear.z = vz
        self.odometry.twist.twist.angular.x = wx
        self.odometry.twist.twist.angular.y = wy
        self.odometry.twist.twist.angular.z = wz

        return self.odometry


    # receive data structure
    # 协议头      | 协议版本 | 头长度 | 报文序号 |  报文类型 | 数据长度    | 保留 | json区 | CRC32
    # 0xaa55aa55 | 0x01   | 0x14  | 0x0001  | 0x8002  | 0x000000cf | 0   | xxx    | 0x689d0b5c

    def only_state(self, data):
        length_data = data[10:14]
        msg_length = int(length_data[::-1].hex(), 16)
        # print('Received {} byte: {}'.format(len(data), data[20: 16 + msg_length]))
        unpacked_values = str(data[20:16 + msg_length], 'utf-8')
        # {"robot_localization_state":0}
        json_values = json.loads(unpacked_values)

        self.slam_state.data = json_values['robot_localization_state']

        return self.slam_state

    def only_laser_scan(self, data):
        length_data = data[10:14]
        msg_length = int(length_data[::-1].hex(), 16)
        print('Received {} byte: {}'.format(len(data), data[20: 16 + msg_length]))
        unpacked_values = str(data[20:16 + msg_length], 'utf-8')
        json_values = json.loads(unpacked_values)
        laser_0 = json_values['lasers'][0]

        # std_msgs/Header header
        #   uint32 seq
        #   time stamp
        #   string frame_id
        # float32 angle_min
        # float32 angle_max
        # float32 angle_increment
        # float32 time_increment
        # float32 scan_time
        # float32 range_min
        # float32 range_max
        # float32[] ranges
        # float32[] intensities
        self.laser_scan.header.stamp = rospy.Time.now()
        self.laser_scan.header.frame_id = laser_0['header']['frame_id']
        self.laser_scan.angle_min = laser_0['device_info']['min_angle']
        self.laser_scan.angle_max = laser_0['device_info']['max_angle']
        self.laser_scan.range_min = laser_0['device_info']['min_range']
        self.laser_scan.range_max = laser_0['device_info']['max_range']
        self.laser_scan.time_increment = laser_0['device_info']['time_increment']
        self.laser_scan.scan_time = 1 / float(laser_0['device_info']['scan_freq'])

        scan_data = laser_0['beams']
        angle_range = self.laser_scan.angle_max - self.laser_scan.angle_min  # TODO
        self.laser_scan.angle_increment = float(angle_range) / len(scan_data)
        self.laser_scan.ranges = [pdata['dist'] for pdata in scan_data]
        self.laser_scan.intensities = [pdata['intensity'] for pdata in scan_data]

        return self.laser_scan

    def only_match(self, data):
        length_data = data[10:14]
        msg_length = int(length_data[::-1].hex(), 16)
        # print('Received {} byte: {}'.format(len(data), data[20: 16 + msg_length]))
        unpacked_values = str(data[20:16 + msg_length], 'utf-8')
        json_values = json.loads(unpacked_values)

        self.match_score.data = json_values['match_score']

        return self.match_score

    def merged_data(self, data):
        unpacked_values = struct.unpack('<QfffffffffffffdB', data[20:20 + 69])

        # example data
        # {"timestamp":1344509504,
        # "x":5.456111,
        # "y":6.345611,
        # "z":1.123456,
        # "qx":1.000000,
        # "qy":3.000000,
        # "qz":5.000000,
        # "qw":6.666677,
        # "vx":4.111111,
        # "vy":5.344500,
        # "vz":2.000000,
        # "wx":1.000000,
        # "wy":2.000000,
        # "wz":3.000000,
        # "match_score":0.70707032,
        # "robot_localization_state":0}
        timestamp, x, y, z, qx, qy, qz, qw, vx, vy, vz, wx, wy, wz, score_data, state_data = unpacked_values
        self.monitor_timestamp(timestamp)

        # TODO: why do not use timestamp from data
        self.odometry.header.stamp = rospy.Time.now()
        self.odometry.header.frame_id = slam_frame_id
        self.odometry.child_frame_id = self.frame_id
        self.odometry.pose.pose.position.y = y
        self.odometry.pose.pose.position.z = z
        self.odometry.pose.pose.orientation.x = qx
        self.odometry.pose.pose.orientation.y = qy
        self.odometry.pose.pose.orientation.z = qz
        self.odometry.pose.pose.position.x = x
        self.odometry.pose.pose.orientation.w = qw
        self.odometry.twist.twist.linear.x = vx
        self.odometry.twist.twist.linear.y = vy
        self.odometry.twist.twist.linear.z = vz
        self.odometry.twist.twist.angular.x = wx
        self.odometry.twist.twist.angular.y = wy
        self.odometry.twist.twist.angular.z = wz

        self.slam_state.data = state_data
        self.match_score.data = score_data

        return self.odometry, self.slam_state, self.match_score

    def merged_data_with_dtc(self, data):
        unpacked_values = struct.unpack('<QfffffffffffffdBIIIIIIIIII', data[20:20 + 109])

        # example data
        # {"timestamp":1344509504,
        # "x":5.456111,
        # "y":6.345611,
        # "z":1.123456,
        # "qx":1.000000,
        # "qy":3.000000,
        # "qz":5.000000,
        # "qw":6.666677,
        # "vx":4.111111,
        # "vy":5.344500,
        # "vz":2.000000,
        # "wx":1.000000,
        # "wy":2.000000,
        # "wz":3.000000,
        # "match_score":0.70707032,
        # "robot_localization_state":0}
        timestamp, x, y, z, qx, qy, qz, qw, vx, vy, vz, wx, wy, wz, score_data, state_data = unpacked_values[:-10]
        self.monitor_timestamp(timestamp)
        dtc_list = unpacked_values[-10:]

        self.odometry.header.stamp = rospy.Time.now()
        self.odometry.header.frame_id = slam_frame_id
        self.odometry.child_frame_id = self.frame_id
        self.odometry.pose.pose.position.y = y
        self.odometry.pose.pose.position.z = z
        self.odometry.pose.pose.orientation.x = qx
        self.odometry.pose.pose.orientation.y = qy
        self.odometry.pose.pose.orientation.z = qz
        self.odometry.pose.pose.position.x = x
        self.odometry.pose.pose.orientation.w = qw
        self.odometry.twist.twist.linear.x = vx
        self.odometry.twist.twist.linear.y = vy
        self.odometry.twist.twist.linear.z = vz
        self.odometry.twist.twist.angular.x = wx
        self.odometry.twist.twist.angular.y = wy
        self.odometry.twist.twist.angular.z = wz

        self.slam_state.data = state_data
        self.match_score.data = score_data

        self.dtc_list.dtc.clear()
        for dtc in dtc_list:
            if dtc != 0:
                self.dtc_list.dtc.append(dtc)
        return self.odometry, self.slam_state, self.match_score, self.dtc_list
