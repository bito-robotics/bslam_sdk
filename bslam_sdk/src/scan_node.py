#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# CheckParam()
# DealData()
# PackResult()
# Clear()

NAME = 'scan_node'

import rospy
import json
import struct
import math
import config
from crccheck.crc import Crc32
from sensor_msgs.msg import LaserScan
from bslam_sdk.msg import *

from tcp_client import SlamTcpClient
from tcp_to_topic import DealData


class ScanPub():
    def __init__(self):
        try:
            self.frame_id = rospy.get_param('/scan_node/frame_id') 
            self.server_ip = rospy.get_param('/scan_node/server_ip')
            self.pub_laser_scan = rospy.get_param('/scan_node/pub_laser_scan')

        except Exception as e:
            rospy.logerr(rospy.get_param_names())
            raise Exception(e)

        self.pub_scan = rospy.Publisher(config.laser_scan_topic, LaserScan, queue_size=1,
                                       tcp_nodelay=True)

        self.deal_data_func = DealData(self.frame_id, monitor_data=False)
        self.req_msg = config.laser_scan_req
        self.deal_data = self.deal_data_func.only_laser_scan

        while True:
            try:
                self.tcp_conn = SlamTcpClient(self.server_ip)
                self.tcp_conn.send_data(self.req_msg)
                break
            except OSError :
                rospy.logerr('Cannot connect to tcp server {}, retry ...'.format(self.server_ip))
                continue

    def transform_data(self):
        data = self.tcp_conn.recv_data()
        scan_msg = self.deal_data(data)
        self.pub_laser_scan.publish(scan_msg)

def main():
    rospy.loginfo('Initialize node ...')
    rospy.init_node(NAME)

    rospy.loginfo('check ros params and init slam client')
    scan_client = ScanPub()

    while not rospy.is_shutdown():
        scan_client.transform_data()
        try:
            scan_client.transform_data()
        except Exception as e:
            rospy.logwarn(e)
            continue

if __name__ == '__main__':
    main()
