#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

continue_port = 19000
single_port = 19001
feedback_port = 19002

slam_frame_id = 'map'
slam_pose_topic = '/odom/filtered'
match_score_topic = '/match_score'
slam_state_topic = '/localization_state'
dtc_topic = '/dtc_data'
laser_scan_topic = '/scan'
map_name_topic = '/map_name'
odom_topic = '/odom/wheel'
imu_topic = '/imu'

slam_state_num = [0,  # 正常
                  1,  # 重定位中
                  2,  # 器人在切换SLAM模式中
                  3,  # 机器人未准备好，是定位启动时候的初始状态，收到传感器数据之后就切换到其他状态
                  4,  # 机器人传感器数据超时异常，当传感器数据恢复时，状态切换为0
                  5]  # 机器人切换地图中

### for localization data req
# 协议头      | 协议版本 | 头长度 | 报文序号 | 报文类型 | 数据长度    | 保留 | CRC32
# 0xaa55aa55 | 0x01   | 0x14  | 0x0001  | 0x0002 | 0x00000004 | 0   | 0xfe9f2a6b

slam_pose_req = \
    b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x02\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x6b\x2a\x9f\xfe'

# 协议头      | 协议版本 | 头长度 | 报文序号 | 报文类型 | 数据长度    | 保留 | CRC32
# 0xaa55aa55 | 0x01   | 0x14  | 0x0001  | 0x0003 | 0x00000004 | 0   | 0x653a6604

slam_state_req = \
    b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x03\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x66\x3a\x65'

match_score_req = \
    b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x07\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x7b\x5d\x3c\x66'

merged_data_req = \
    b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x08\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0a\xc2\xa8\x14'

merged_data_with_dtc_req = \
    b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x09\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x65\x8e\x0d\x8f'

laser_scan_req = \
    b'\x55\xaa\x55\xaa\x01\x14\x01\x00\x06\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x14\x11\x99\xfd'