#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup()
d['packages'] = ['bslam_sdk']
d['package_dir'] = {'': 'src'}

setup(**d)