BSLAM ROS SDK
=============
此包用于测试bslam的tcp接口，协议通信方式采用TCP方式，bslam_sdk作为一个TCP客户端与SLAM模组服务端进行通信.
bslam的tcp接口文档可以查看[文档](share/doc/上海宾通BSLAM通信协议（TCP版）v1.3 2021.04.09.docx)

How to build bslam_sdk ros package
==================================
    1) install dependences: $ sudo pip3 install PyYAML rospkg crccheck requrests
    2) Clone this project to your catkin's workspace src folder
    3) Running catkin_make to build bslam_sdk package

How to config bslam_sdk ros package
===================================
There're two ways to config bslam_sdk ros package

I. config topic names and port through src/config.py
----------------------------------------------------
bslam server port: the bslam server port provided
slam_frame_id: this is the base frame id in pose msg
slam_state_num: the slam state number publish by /localization_state
req_msg: different req_msg defines, bslam_sdk will auto choose req_msg send to server.

II. config topic names and port through launch/node_bslam_sdk.launch
--------------------------------------------------------------------
server_ip: the slam server ip you can connect to
args: you can choose the topic need to pub, now it only approve the combination list at doc
remap: you can also remap pub/sub topics name here
if set arg ${use_monitor}, node will check the timestamp in msg and receive time. log warning.

How to run bslam_sdk ros package
===================================
  1) source {path_to_catkin_ws}/devel/setup.bash
  2) roslaunch bslam_sdk node_bslam_sdk.launch

